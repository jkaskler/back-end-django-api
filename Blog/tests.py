from django.contrib.auth.models import User
from rest_framework.test import APITestCase, APIClient
from rest_framework import status
from Blog.models import Article, Comment


# Create your tests here.
class ArticleCommentTests(APITestCase):
    multi_db = True

    def setUp(self) -> None:
        self.user = User.objects.create_superuser('Jake')

    def testCreateArticleNoAuth(self):
        url = '/article/'
        data = {"title": "SecondArticle", "content": "TestAPI content"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def testCreateArticleAuth(self):
        url = '/article/'
        data = {"title": "SecondArticle", "content": "TestAPI content"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Article.objects.count(), 1)
        self.assertEqual(Article.objects.get().title, 'SecondArticle')

    def testGetArticle(self):
        url = '/article/'
        data = {"title": "ThisIsAnArticle", "content": "TestAPI content"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/article/1/'
        response = self.client.get(url)
        self.assertEqual(Article.objects.get().id, 1)

    def testListArticle(self):
        url = '/article/'
        data = {"title": "ThisIsAnArticle", "content": "TestAPI content"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {"title": "ThisIsA2ndArticle", "content": "TestAPI content2"}
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/article/'
        response = self.client.get(url)
        self.assertEqual(Article.objects.count(), 2)

    def testCreateCommentAuth(self):
        url = '/article/'
        data = {"title": "ThisIsAnArticle", "content": "TestAPI content"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/comment/1/'
        data = {"comment": "This is a comment"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def testDetailComment(self):
        url = '/article/'
        data = {"title": "ThisIsAnArticle", "content": "TestAPI content"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/comment/1/'
        data = {"comment": "This is a comment"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/comment/id/1'
        response = self.client.get(url)
        self.assertEqual(Comment.objects.get().id, 1)

    def testDeleteCommentAuth(self):
        url = '/article/'
        data = {"title": "ThisIsAnArticle", "content": "TestAPI content"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/comment/1/'
        data = {"comment": "This is a comment"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/comment/id/1/'
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def testDeleteArticleNoAuth(self):
        url = '/article/'
        data = {"title": "SecondArticle", "content": "TestAPI content"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/article/1/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def testDeleteArticleAuth(self):
        url = '/article/'
        data = {"title": "SecondArticle", "content": "TestAPI content"}
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = '/article/1/'
        user = User.objects.get(username='Jake')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Article.objects.count(), 0)

