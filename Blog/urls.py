from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from Blog import views


urlpatterns = [
    path('article/', views.ArticleList),
    path('article/<int:pk>/', views.ArticleDetail),
    path('comment/', views.CommentList),
    path('comment/<int:article>/', views.ArticleCommentList),
    path('comment/id/<int:pk>/', views.CommentDetail),
]

urlpatterns = format_suffix_patterns(urlpatterns)
