from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from Blog.models import Article, Comment
from Blog.serializer import ArticleSerializer, CommentSerializer, \
                            ManyCommentsSerializer


# Create your views here.
#
# class ArticleList(generics.ListCreateAPIView):
#     queryset = Article.objects.all()
#     serializer_class = ArticleSerializer


# class ArticleDetail(generics.RetrieveDestroyAPIView):
#     queryset = Article.objects.all()
#     serializer_class = ArticleSerializer


# class CommentList(generics.ListAPIView):
#     queryset = Article.objects.all()
#     serializer_class = ManyCommentsSerializer


@api_view(['GET', 'POST'])
def ArticleList(request, format=None):
    if request.method == 'GET':
        article = Article.objects.all()
        serializer = ArticleSerializer(article, many=True)
        return Response(serializer.data)

    if request.method == 'POST':
        if request.user.is_staff:
            data = request.data
            data.update({'created_date': timezone.now})
            data.update({'last_modified_date': timezone.now})
            data.update({'author': str(request.user)})
            serializer = ArticleSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            return Response(serializer.data,
                            status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET', 'POST'])
def ArticleCommentList(request, article, format=None):
    if request.method == 'GET':
        comment = Comment.objects.filter(article=article)
        serializer = CommentSerializer(comment, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        data = request.data
        data.update({'article': article})
        data.update({'username': str(request.user)})
        serializer = CommentSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def CommentList(request, format=None):
    if request.method == 'GET':
        comment = Article.objects.all()
        serializer = ManyCommentsSerializer(comment, many=True)
        return Response(serializer.data)


@api_view(['GET', 'DELETE'])
def ArticleDetail(request, pk, format=None):
    try:
        article = Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ArticleSerializer(article)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        if request.user.is_staff:
            article.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET', 'DELETE'])
def CommentDetail(request, pk, format=None):
    try:
        comment = Comment.objects.get(pk=pk)
    except Comment.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CommentSerializer(comment)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        if request.user.is_staff:
            comment.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_401_UNAUTHORIZED)

# class ArticleCommentList(generics.ListCreateAPIView):
#     serializer_class = ManyCommentsSerializer
#
#     def get_queryset(self):
#         articleID = self.kwargs['article']
#         return Article.objects.filter(id=articleID)
#
#
# class CommentDetail(generics.RetrieveUpdateDestroyAPIView):
#     serializer_class = CommentSerializer
#
#     def get_queryset(self):
#         commentNo = self.kwargs['comment']
#         return Article.objects.all()
