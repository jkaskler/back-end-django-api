from django.db import models


# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    created_date = models.DateTimeField(auto_now=True)
    last_modified_date = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=100)


class Comment(models.Model):
    article = models.ForeignKey(Article, related_name='comments',
                                on_delete=models.CASCADE)
    comment = models.TextField()
    username = models.CharField(max_length=100)