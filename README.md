This project is a Back-End API using django that will allow POST, PULL and DELETE of Articles and Comments that can be communicated with from a Front-End developed blog page.

To use:
POST Articles to ~/article/ using the Json format
{
        "id": "", - Auto Filled
        "title": "___",
        "content": "___",
        "createdDate": "", - Auto Filled
        "lastModifiedDate": "", - Auto Filled
        "author": "" - Auto Filled
    }
PULL Articles from ~/article/

DELETE Articles from ~/article/(ArticleID)/

POST Comments to ~/comment/(ArticleID)/ using the Json format
{
    "id": "", - Auto Filled
    "article": "", - Auto Filled
    "comment": "___",
    "username": "" - Auto Filled
}
PULL all Comments from ~/comment/

PULL Comments only from specific Article ~/comment/(ArticleID)/

PULL Comment by ID from ~/comment/id/(commentID)/

DELETE Comments from ~/comment/id/(commentID)/
